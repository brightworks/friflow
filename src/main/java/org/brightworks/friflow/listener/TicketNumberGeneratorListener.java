package org.brightworks.friflow.listener;

import org.brightworks.friflow.domain.process.collection.Collection;
import org.brightworks.friflow.domain.process.delivery.Delivery;
import org.brightworks.friflow.domain.process.production.Production;
import org.brightworks.friflow.domain.process.quotation.Quotation;
import org.joda.time.LocalDateTime;

import javax.persistence.PrePersist;

/**
 * @author kyel
 */
public class TicketNumberGeneratorListener {

    private static final String TICKET_DATE_FORMAT = "mmddyyssa";

    @PrePersist
    public void onPrePersist(Object o) {
        if(o instanceof Quotation){
            ((Quotation) o).setTicketNumber("Q"+
                    LocalDateTime.now()
                            .toString(TICKET_DATE_FORMAT));
            ((Quotation) o).setDateTimeCreated(LocalDateTime.now());
        }

        if(o instanceof Production){
            ((Production) o).setTicketNumber("P"+
                    LocalDateTime.now()
                            .toString(TICKET_DATE_FORMAT));
            ((Production) o).setDateTimeCreated(LocalDateTime.now());
        }

        if(o instanceof Collection){
            ((Collection) o).setTicketNumber("C"+
                    LocalDateTime.now()
                            .toString(TICKET_DATE_FORMAT));
            ((Collection) o).setDateTimeCreated(LocalDateTime.now());
        }

        if(o instanceof Delivery){
            ((Delivery) o).setTicketNumber("D"+
                    LocalDateTime.now()
                            .toString(TICKET_DATE_FORMAT));
            ((Delivery) o).setDateTimeCreated(LocalDateTime.now());
        }
    }

}

package org.brightworks.friflow.domain.dto;

/**
 * @author kyel
 */
public class CollectionDTO {

    private Long id;

    private String ticketNumber;

    private String processStatus;

    private String customerName;

    private String description;

    private String dateTimeCreated;

    private String targetDate;

    private String invoiceNumber;

    private String invoiceDate;

    private String paymentTerm;

    private boolean forCollection;

    private String deliveryTicketNumber;

    private long version;

    private Double amount;

    private boolean editable;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setDateTimeCreated(String dateTimeCreated) {
        this.dateTimeCreated = dateTimeCreated;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(String targetDate) {
        this.targetDate = targetDate;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public boolean isForCollection() {
        return forCollection;
    }

    public void setForCollection(boolean forCollection) {
        this.forCollection = forCollection;
    }

    public String getDeliveryTicketNumber() {
        return deliveryTicketNumber;
    }

    public void setDeliveryTicketNumber(String deliveryTicketNumber) {
        this.deliveryTicketNumber = deliveryTicketNumber;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}

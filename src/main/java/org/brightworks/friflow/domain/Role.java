package org.brightworks.friflow.domain;

/**
 * author kyel
 */
public enum Role {

    ROLE_ADMIN("Admin"),
    ROLE_QUOTATION_ADD("Quotation - ADD"),
    ROLE_QUOTATION_VIEW("Quotation - VIEW"),
    ROLE_QUOTATION_EDIT("Quotation - EDIT"),
    ROLE_PRODUCTION_ADD("Production - ADD"),
    ROLE_PRODUCTION_EDIT("Production - EDIT"),
    ROLE_PRODUCTION_VIEW("Production - VIEW"),
    ROLE_DELIVERY_ADD("Delivery - ADD"),
    ROLE_DELIVERY_VIEW("Delivery - VIEW"),
    ROLE_DELIVERY_EDIT("Delivery - EDIT"),
    ROLE_COLLECTION_ADD("Collection - ADD"),
    ROLE_COLLECTION_VIEW("Collection - VIEW"),
    ROLE_COLLECTION_EDIT("Collection - EDIT");

    Role(String code){
        this.code = code;
    }

    private String code;

    public String getCode() {
        return code;
    }
}

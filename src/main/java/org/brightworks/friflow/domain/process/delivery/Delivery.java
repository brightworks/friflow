package org.brightworks.friflow.domain.process.delivery;

import org.brightworks.friflow.domain.process.BaseProcess;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author kyel
 */
@Entity
@Table(name = "TXN_DELIVERY")
public class Delivery extends BaseProcess {

    @Column(name = "description")
    private String description;

    @Column(name = "prod_ticket_number")
    private String productionTicketNumber;


    @Column(name = "sales_invoice_number")
    private String invoiceNumber;

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductionTicketNumber() {
        return productionTicketNumber;
    }

    public void setProductionTicketNumber(String productionTicketNumber) {
        this.productionTicketNumber = productionTicketNumber;
    }
}

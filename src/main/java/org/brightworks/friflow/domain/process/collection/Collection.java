package org.brightworks.friflow.domain.process.collection;

import org.brightworks.friflow.domain.process.BaseProcess;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author kyel
 */
@Entity
@Table(name = "TXN_COLLECTION")
public class Collection extends BaseProcess{

    @Column(name = "description")
    private String description;

    @Column(name = "sales_invoice_number")
    private String invoiceNumber;

    @Column(name = "sales_invoice_date")
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate invoiceDate;

    @Column(name = "payment_term")
    @Enumerated(EnumType.STRING)
    private PaymentTerm paymentTerm;

    @Column(name = "for_collection")
    private boolean forCollection;

    @Column(name = "delivery_ticket_number")
    private String deliveryTicketNumber;

    @Column(name = "amount")
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public LocalDate getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(LocalDate invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public PaymentTerm getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(PaymentTerm paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public boolean isForCollection() {
        return forCollection;
    }

    public void setForCollection(boolean forCollection) {
        this.forCollection = forCollection;
    }

    public String getDeliveryTicketNumber() {
        return deliveryTicketNumber;
    }

    public void setDeliveryTicketNumber(String deliveryTicketNumber) {
        this.deliveryTicketNumber = deliveryTicketNumber;
    }

    @Override
    public String toString() {
        return "Collection{" +
                "description='" + description + '\'' +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", invoiceDate=" + invoiceDate +
                ", paymentTerm=" + paymentTerm +
                ", forCollection=" + forCollection +
                ", deliveryTicketNumber='" + deliveryTicketNumber + '\'' +
                "} " + super.toString();
    }
}

package org.brightworks.friflow.domain.process;

/**
 * @author kyel
 */
public enum ProcessStatus {
    PENDING,
    COMPLETED,
    ACCEPTED
}

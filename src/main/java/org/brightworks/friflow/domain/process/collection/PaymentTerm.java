package org.brightworks.friflow.domain.process.collection;

/**
 * @author kyel
 */
public enum PaymentTerm {
    NEW_CUSTOMER,
    OLD_CUSTOMER
}

package org.brightworks.friflow.mapper.impl;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.brightworks.friflow.domain.Name;
import org.brightworks.friflow.domain.UserAccount;
import org.brightworks.friflow.domain.dto.NameDTO;
import org.brightworks.friflow.domain.dto.UserAccountDTO;
import org.springframework.stereotype.Component;

/**
 * Created by kyel on 2/3/2016.
 */
@Component
public class UserAccountMapper extends CustomMapper<UserAccount, UserAccountDTO> {

    public void mapAtoB(UserAccount user, UserAccountDTO dto,MappingContext context){
        NameDTO nameDTO = new NameDTO();
        if(user.getName() != null) {
            nameDTO.setMiddleName(user.getName().getMiddleName());
            nameDTO.setFamilyName(user.getName().getFamilyName());
            nameDTO.setGivenName(user.getName().getGivenName());
            dto.setName(nameDTO);
        }
        dto.setRole(user.getRoles());
    }

    public void mapBtoA(UserAccountDTO dto, UserAccount user, MappingContext context) {
        user.setRoles(dto.getRole());
        Name name = new Name();
        name.setFamilyName(dto.getName().getFamilyName());
        name.setGivenName(dto.getName().getGivenName());
        name.setMiddleName(dto.getName().getMiddleName());
        user.setName(name);
    }
}

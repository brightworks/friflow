package org.brightworks.friflow.mapper.impl;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.brightworks.friflow.domain.CompanyName;
import org.brightworks.friflow.domain.dto.DeliveryDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.delivery.Delivery;
import org.brightworks.friflow.repo.CompanyNameRepo;
import org.brightworks.friflow.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by kyeljohndavid on 12/14/2015.
 */
@Component
public class DeliveryMapper extends CustomMapper<Delivery, DeliveryDTO> {

    @Autowired
    private CompanyNameRepo companyNameRepo;

    public void mapAtoB(Delivery delivery, DeliveryDTO deliveryDTO, MappingContext context) {
        deliveryDTO.setCustomerName(delivery.getClientName().getName());
        deliveryDTO.setTargetDate(DateUtil.formatDate(delivery.getTargetDate()));
        deliveryDTO.setProcessStatus(delivery.getProcessStatus().toString());
        deliveryDTO.setInvoiceNumber(delivery.getInvoiceNumber());
        deliveryDTO.setDateTimeCreated(DateUtil.formatDateTime(delivery.getDateTimeCreated(), "MM-dd-yyyy hh:mm aa"));
        deliveryDTO.setTargetDate(DateUtil.formatDate(delivery.getTargetDate()));
        if(delivery.getProcessStatus() != null
                && (delivery.getProcessStatus()
                .equals(ProcessStatus.COMPLETED))) {
            deliveryDTO.setEditable(false);
        }
    }

    public void mapBtoA(DeliveryDTO dto, Delivery delivery, MappingContext context) {
        setDeliveryClientName(dto, delivery);
        delivery.setInvoiceNumber(dto.getInvoiceNumber());
        delivery.setTargetDate(DateUtil.toLocalDate(dto.getTargetDate()));
    }

    private void setDeliveryClientName(DeliveryDTO dto, Delivery delivery) {
        if(doesCompanyNameExist(dto.getCustomerName())){
            delivery.setClientName(companyNameRepo.findByName(dto.getCustomerName().toUpperCase()));
        }else{
            CompanyName companyName = new CompanyName();
            companyName.setName(dto.getCustomerName().toUpperCase());
            delivery.setClientName(companyNameRepo.save(companyName));
        }
    }

    private boolean doesCompanyNameExist(String companyName){
        return (companyNameRepo.findByName(companyName.toUpperCase()) != null);
    }
}

package org.brightworks.friflow.mapper.impl;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MappingContext;
import org.brightworks.friflow.domain.CompanyName;
import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.collection.Collection;
import org.brightworks.friflow.domain.process.collection.PaymentTerm;
import org.brightworks.friflow.repo.CompanyNameRepo;
import org.brightworks.friflow.util.DateUtil;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @author kyel
 */
@Component
public class CollectionMapper extends CustomMapper<Collection, CollectionDTO> {

    @Autowired
    private CompanyNameRepo companyNameRepo;

    public void mapAtoB(Collection collection, CollectionDTO collectionDTO, MappingContext context) {
        collectionDTO.setCustomerName(collection.getClientName().getName());
        collectionDTO.setTargetDate(DateUtil.formatDate(collection.getTargetDate()));
        collectionDTO.setProcessStatus(collection.getProcessStatus().toString());
        collectionDTO.setDateTimeCreated(DateUtil.formatDateTime(collection.getDateTimeCreated(), "MM-dd-yyyy hh:mm aa"));
        collectionDTO.setInvoiceDate(DateUtil.formatDate(collection.getTargetDate()));
        collectionDTO.setAmount(collection.getAmount().doubleValue());
        if(collection.getProcessStatus() != null
                && (collection.getProcessStatus()
                .equals(ProcessStatus.COMPLETED))) {
            collectionDTO.setEditable(false);
            collectionDTO.setForCollection(false);
        }

        if(collection.getTargetDate().isBefore(LocalDate.now())) {
            collectionDTO.setForCollection(true);
        }
    }

    public void mapBtoA(CollectionDTO collectionDTO, Collection collection, MappingContext context) {
        setClientName(collectionDTO,collection);
        collection.setAmount(new BigDecimal(collectionDTO.getAmount()));
        collection.setInvoiceDate(DateUtil.toLocalDate(collectionDTO.getInvoiceDate()));
        collection.setTargetDate(DateUtil.toLocalDate(collectionDTO.getTargetDate()));
        collection.setPaymentTerm(PaymentTerm.valueOf(collectionDTO.getPaymentTerm()));

        if(collection.getTargetDate().isBefore(LocalDate.now())) {
            collection.setForCollection(true);
        }else {
            collection.setForCollection(false);
        }
    }

    private void setClientName(CollectionDTO dto, Collection delivery) {
        if(doesCompanyNameExist(dto.getCustomerName())){
            delivery.setClientName(companyNameRepo.findByName(dto.getCustomerName().toUpperCase()));
        }else{
            CompanyName companyName = new CompanyName();
            companyName.setName(dto.getCustomerName().toUpperCase());
            delivery.setClientName(companyNameRepo.save(companyName));
        }
    }

    private boolean doesCompanyNameExist(String companyName){
        return (companyNameRepo.findByName(companyName.toUpperCase()) != null);
    }
}

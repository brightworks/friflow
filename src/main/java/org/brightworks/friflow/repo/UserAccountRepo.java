package org.brightworks.friflow.repo;

import org.brightworks.friflow.domain.UserAccount;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author kyel
 */
public interface UserAccountRepo extends PagingAndSortingRepository<UserAccount,Long>,UserAccountRepoCustom{

    UserAccount findByUsername(String username);

}

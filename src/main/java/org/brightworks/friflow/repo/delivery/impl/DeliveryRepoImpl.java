package org.brightworks.friflow.repo.delivery.impl;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import org.brightworks.friflow.domain.dto.DeliveryDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.delivery.Delivery;
import org.brightworks.friflow.domain.process.delivery.QDelivery;
import org.brightworks.friflow.mapper.OrikaBeanMapper;
import org.brightworks.friflow.repo.delivery.DeliveryRepoCustom;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kyel
 */
public class DeliveryRepoImpl implements DeliveryRepoCustom {

    @Autowired
    private EntityManager em;

    @Autowired
    private OrikaBeanMapper mapper;

    @Override
    public Page<DeliveryDTO> search(String ticketNumber,
                                      String description,
                                      String clientName,
                                      LocalDate startDate,
                                      LocalDate endDate,
                                    ProcessStatus processStatus,
                                      Pageable pageable) {


        JPAQuery query = new JPAQuery(em);
        QDelivery delivery =  QDelivery.delivery;
        BooleanBuilder expression = null;
        expression = expressionBuilder(ticketNumber, description, clientName, startDate, endDate,processStatus, delivery);
        List<Delivery> results = queryBuilder(pageable, query, delivery, expression);
        List<DeliveryDTO> res = new ArrayList<>();
        for(Delivery col : results){
            DeliveryDTO deliveryDTO = new DeliveryDTO();
            mapper.map(col, deliveryDTO);
            res.add(deliveryDTO);
        }



        if(pageable != null){
            return new PageImpl<>(res,pageable,query.count());
        }else{
            return new PageImpl<>(res);
        }
    }

    private List<Delivery> queryBuilder(Pageable pageable, JPAQuery query, QDelivery delivery,
                                         BooleanBuilder expression) {

        if(pageable != null){
            return query.from(delivery)
                    .where(expression)
                    .limit(pageable.getPageSize())
                    .offset(pageable.getOffset())
                    .orderBy(delivery.targetDate.asc())
                    .list(delivery);
        }else{
            return query.from(delivery)
                    .where(expression)
                    .orderBy(delivery.dateTimeCreated.asc())
                    .list(delivery);
        }
    }

    private BooleanBuilder expressionBuilder(String ticketNumber,
                                             String description,
                                             String clientName,
                                             LocalDate startDate,
                                             LocalDate endDate,
                                             ProcessStatus processStatus,
                                             QDelivery delivery) {

        BooleanBuilder booleanBuilder = new BooleanBuilder();

        if(ticketNumber != null && !ticketNumber.isEmpty()){
            booleanBuilder.or(delivery.ticketNumber.contains(ticketNumber));
        }

        if(description != null && !description.isEmpty()){
            booleanBuilder.or(delivery.description.contains(description));
        }

        if(clientName !=null && !clientName.isEmpty()){
            booleanBuilder.or(delivery.clientName.name.contains(clientName));
        }

        //Append date if start date and end date is not equal to null
        if(startDate != null  && endDate != null){
            booleanBuilder.and(delivery.targetDate.goe(startDate).and(delivery.targetDate.loe(endDate)));
        }

        if(processStatus != null){
            booleanBuilder.and(delivery.processStatus.eq(processStatus));
        }

        booleanBuilder.and(delivery.deleted.eq(false));

        return booleanBuilder;
    }

}

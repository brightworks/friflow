package org.brightworks.friflow.repo.delivery;

import org.brightworks.friflow.domain.process.delivery.Delivery;
import org.brightworks.friflow.repo.ParentProcessRepo;

/**
 * @author kyel
 */
public interface DeliveryRepo extends ParentProcessRepo<Delivery,Long>,DeliveryRepoCustom {

}

package org.brightworks.friflow.repo.delivery;

import org.brightworks.friflow.domain.dto.DeliveryDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.joda.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author kyel
 */
public interface DeliveryRepoCustom {


    Page<DeliveryDTO> search(String ticketNumber,
                              String description,
                              String clientName,
                              LocalDate startDate,
                              LocalDate endDate,
                              ProcessStatus processStatus,
                              Pageable pageable);
}

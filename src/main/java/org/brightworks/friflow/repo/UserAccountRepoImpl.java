package org.brightworks.friflow.repo;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import org.brightworks.friflow.domain.QUserAccount;
import org.brightworks.friflow.domain.Role;
import org.brightworks.friflow.domain.UserAccount;
import org.brightworks.friflow.domain.dto.UserAccountDTO;
import org.brightworks.friflow.mapper.OrikaBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyel on 1/12/2016.
 */
public class UserAccountRepoImpl implements UserAccountRepoCustom {


    @Autowired
    private EntityManager em;

    @Autowired
    private OrikaBeanMapper mapper;

    @Override
    public Page<UserAccountDTO> search(String username, String name, Role role, Pageable pageable) {
        JPAQuery query = new JPAQuery(em);
        QUserAccount userAccount = QUserAccount.userAccount;

        BooleanBuilder builder = new BooleanBuilder();

        if(username != null){
           builder.and(userAccount.username.containsIgnoreCase(username));
        }

        if(name != null){
            builder.and(userAccount.name.givenName.contains(name).or(userAccount.name.familyName.contains(name)));
        }

        builder.and(userAccount.roles.contains(role));

        List<UserAccount> accountList = query
                .from(userAccount)
                .where(builder)
                .limit(pageable.getPageSize())
                .offset(pageable.getOffset())
                .list(userAccount);

        List<UserAccountDTO> res = new ArrayList<>();
        for(UserAccount account : accountList){
            res.add(mapper.map(account,UserAccountDTO.class));
        }

        return new PageImpl<>(res,pageable,query.count());
    }
}

package org.brightworks.friflow.repo.collection;

import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.collection.PaymentTerm;
import org.joda.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author kyel
 */
public interface CollectionRepoCustom {

    Page<CollectionDTO> search(String invoiceNumber,
                               String description,
                               String clientName,
                               LocalDate startDate,
                               LocalDate endDate,
                               ProcessStatus status,
                               PaymentTerm paymentTerm,
                               Pageable pageable);


    Page<CollectionDTO> searchUseDateCreated(String invoiceNumber,
                               String description,
                               String clientName,
                               LocalDate startDate,
                               LocalDate endDate,
                               ProcessStatus status,
                               PaymentTerm paymentTerm,
                               Pageable pageable);
}

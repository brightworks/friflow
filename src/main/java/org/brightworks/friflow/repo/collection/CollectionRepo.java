package org.brightworks.friflow.repo.collection;

import org.brightworks.friflow.domain.process.collection.Collection;
import org.brightworks.friflow.repo.ParentProcessRepo;

/**
 * @author kyel
 */
public interface CollectionRepo extends ParentProcessRepo<Collection,Long>,CollectionRepoCustom {
}

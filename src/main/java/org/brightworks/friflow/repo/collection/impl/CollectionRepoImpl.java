package org.brightworks.friflow.repo.collection.impl;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.jpa.impl.JPAQuery;
import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.collection.Collection;
import org.brightworks.friflow.domain.process.collection.PaymentTerm;
import org.brightworks.friflow.domain.process.collection.QCollection;
import org.brightworks.friflow.mapper.OrikaBeanMapper;
import org.brightworks.friflow.repo.collection.CollectionRepoCustom;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kyel
 */
public class CollectionRepoImpl implements CollectionRepoCustom {

    @Autowired
    private EntityManager em;

    @Autowired
    private OrikaBeanMapper mapper;

    @Override
    public Page<CollectionDTO> search(String invoiceNumber,
                                      String description,
                                      String clientName,
                                      LocalDate startDate,
                                      LocalDate endDate,
                                      ProcessStatus status,
                                      PaymentTerm paymentTerm,
                                      Pageable pageable) {

        JPAQuery query = new JPAQuery(em);
        QCollection collection = QCollection.collection;
        BooleanBuilder expression = expressionBuilder(invoiceNumber, description, clientName, startDate, endDate, status, false,collection);
        List<Collection> results = queryBuilder(pageable, query, collection, expression);
        List<CollectionDTO> res = new ArrayList<>();
        for(Collection col : results){
            CollectionDTO collectionDTO = new CollectionDTO();
            mapper.map(col, collectionDTO);
            res.add(collectionDTO);
        }

        if(pageable != null){
            return new PageImpl<>(res,pageable,query.count());
        }else{
            return new PageImpl<>(res);
        }

    }

    @Override
    public Page<CollectionDTO> searchUseDateCreated(String invoiceNumber, String description, String clientName, LocalDate startDate, LocalDate endDate, ProcessStatus status, PaymentTerm paymentTerm, Pageable pageable) {
        JPAQuery query = new JPAQuery(em);
        QCollection collection = QCollection.collection;
        BooleanBuilder expression = expressionBuilder(invoiceNumber, description, clientName, startDate, endDate, status, true,collection);
        List<Collection> results = queryBuilder(pageable, query, collection, expression);
        List<CollectionDTO> res = new ArrayList<>();
        for(Collection col : results){
            CollectionDTO collectionDTO = new CollectionDTO();
            mapper.map(col, collectionDTO);
            res.add(collectionDTO);
        }

        if(pageable != null){
            return new PageImpl<>(res,pageable,query.count());
        }else{
            return new PageImpl<CollectionDTO>(res);
        }
    }

    private List<Collection> queryBuilder(Pageable pageable, JPAQuery query, QCollection collection,
                                        BooleanBuilder expression) {
        if(pageable != null){
            return query.from(collection)
                    .where(expression)
                    .limit(pageable.getPageSize())
                    .offset(pageable.getOffset())
                    .orderBy(collection.targetDate.asc())
                    .list(collection);
        }else{
            return query.from(collection)
                    .where(expression)
                    .orderBy(collection.dateTimeCreated.asc())
                    .list(collection);
        }

    }

    private BooleanBuilder expressionBuilder(String invoiceNumber,
                                             String description,
                                             String clientName,
                                             LocalDate startDate,
                                             LocalDate endDate,
                                             ProcessStatus status,
                                             boolean useDateCreated,
                                             QCollection collection){
        BooleanBuilder builder = new BooleanBuilder();

        if(invoiceNumber != null && !invoiceNumber.isEmpty()){
            builder.or(collection.invoiceNumber.contains(invoiceNumber));
        }

        if(description != null && !description.isEmpty()){
            builder.or(collection.description.contains(description));
        }

        if(clientName != null && !clientName.isEmpty()){
            builder.or(collection.clientName.name.contains(clientName));
        }

        //Append date if start date and end date is not equal to null
        if(!useDateCreated && startDate != null  && endDate != null){
            builder.and(collection.targetDate.goe(startDate).and(collection.targetDate.loe(endDate)));
        }

        if(useDateCreated && startDate != null  && endDate != null) {
            builder.and(collection.dateTimeCreated.goe(startDate.toLocalDateTime(LocalTime.MIDNIGHT))
                    .and(collection.dateTimeCreated.loe(endDate.toLocalDateTime(LocalTime.MIDNIGHT))));
        }



        if(status != null) {
            builder.and(collection.processStatus.eq(status));
        }

        builder.and(collection.deleted.eq(false));
        return builder;
    }
}

package org.brightworks.friflow.repo;

import org.brightworks.friflow.domain.Role;
import org.brightworks.friflow.domain.dto.UserAccountDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by kyel on 1/12/2016.
 */
public interface UserAccountRepoCustom {

    Page<UserAccountDTO> search(String username,String name, Role role, Pageable pageable);


}

package org.brightworks.friflow.service;

/**
 * Created by kyel on 6/9/2016.
 */
public enum Module {
    PRODUCTION,
    QUOTATION,
    DELIVERY,
    COLLECTION,
}

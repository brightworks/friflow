package org.brightworks.friflow.service;

import org.brightworks.friflow.domain.process.collection.Collection;
import org.brightworks.friflow.domain.process.delivery.Delivery;
import org.brightworks.friflow.domain.process.production.Production;
import org.brightworks.friflow.domain.process.quotation.Quotation;
import org.brightworks.friflow.repo.collection.CollectionRepo;
import org.brightworks.friflow.repo.delivery.DeliveryRepo;
import org.brightworks.friflow.repo.production.ProductionRepo;
import org.brightworks.friflow.repo.quotation.QuotationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by kyel on 6/9/2016.
 */
@Service
public class BaseProcessTicketService {

    @Autowired
    private QuotationRepo quotationRepo;

    @Autowired
    private ProductionRepo productionRepo;

    @Autowired
    private CollectionRepo collectionRepo;

    @Autowired
    private DeliveryRepo deliveryRepo;

    public void deleteTicket(Long ticketId, Module module) {
        switch (module) {
            case COLLECTION:
                Collection collection = collectionRepo.findOne(ticketId);
                collection.setDeleted(true);
                collectionRepo.save(collection);
                break;
            case DELIVERY:
                Delivery delivery = deliveryRepo.findOne(ticketId);
                delivery.setDeleted(true);
                deliveryRepo.save(delivery);
                break;
            case PRODUCTION:
                Production production = productionRepo.findOne(ticketId);
                production.setDeleted(true);
                productionRepo.save(production);
                break;
            case QUOTATION:
                Quotation quotation = quotationRepo.findOne(ticketId);
                quotation.setDeleted(true);
                quotationRepo.save(quotation);
                break;
        }
    }
}

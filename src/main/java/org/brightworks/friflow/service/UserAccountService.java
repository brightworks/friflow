package org.brightworks.friflow.service;

import org.brightworks.friflow.domain.UserAccount;
import org.brightworks.friflow.domain.dto.UserAccountDTO;

import java.util.List;

/**
 * @author kyel
 */
public interface UserAccountService {

    void save(UserAccount user);

    UserAccount findByUsername(String username);

    List<UserAccountDTO> retrieveAll();

    void changePassword(String username, String oldPassword, String newPassword) throws PasswordDoesNotMatchException;

    void update(UserAccount userAccount);
}

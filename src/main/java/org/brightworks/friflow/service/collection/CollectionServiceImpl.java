package org.brightworks.friflow.service.collection;

import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.collection.Collection;
import org.brightworks.friflow.domain.process.collection.PaymentTerm;
import org.brightworks.friflow.mapper.OrikaBeanMapper;
import org.brightworks.friflow.repo.collection.CollectionRepo;
import org.brightworks.friflow.service.FormDataIntegrityException;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author kyel
 */
@Service
public class CollectionServiceImpl  implements CollectionService{

    @Autowired
    private CollectionRepo collectionRepo;

    @Autowired
    private OrikaBeanMapper mapper;

    @Override
    public Collection save(CollectionDTO collectionDTO) throws FormDataIntegrityException {
        Collection collection;
        if(collectionDTO.getId() != null){
            collection = collectionRepo.findOne(collectionDTO.getId());
            if(collection.getVersion() != collectionDTO.getVersion()) throw new FormDataIntegrityException();
            mapper.map(collectionDTO, collection);
        }else{
            collection = mapper.map(collectionDTO,Collection.class);

        }
        collection = collectionRepo.save(collection);
        return collection;
    }

    @Override
    public CollectionDTO findOne(Long id) {
        return mapper.map(collectionRepo.findOne(id),CollectionDTO.class);
    }

    @Override
    public Page<CollectionDTO> search(String invoiceNumber, String description, String clientName, LocalDate startDate, LocalDate endDate,
                                      ProcessStatus status,PaymentTerm paymentTerm, Pageable pageable) {
        return collectionRepo.search(invoiceNumber,description,clientName,startDate,endDate,status,paymentTerm,pageable);
    }

    /*
     ** Retrieves all collection tickets within the given date
     */
    @Override
    public List<CollectionDTO> retrieveCollectionBetweenDates(LocalDate startDate, LocalDate endDate) {
        return collectionRepo.searchUseDateCreated(null,null,null,startDate,endDate,null,null,null).getContent();
    }
}

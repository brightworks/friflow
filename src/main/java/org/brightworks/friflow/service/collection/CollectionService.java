package org.brightworks.friflow.service.collection;

import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.collection.Collection;
import org.brightworks.friflow.domain.process.collection.PaymentTerm;
import org.brightworks.friflow.service.FormDataIntegrityException;
import org.joda.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author kyel
 */
public interface CollectionService {

    Collection save(CollectionDTO collectionDTO) throws FormDataIntegrityException;

    CollectionDTO findOne(Long id);

    Page<CollectionDTO>  search(String invoiceNumber,
                                String description,
                                String clientName,
                                LocalDate startDate,
                                LocalDate endDate,
                                ProcessStatus status,
                                PaymentTerm paymentTerm,
                                Pageable pageable);

    /*
     ** Retrieves all collection tickets within the given date
     */
    List<CollectionDTO> retrieveCollectionBetweenDates(LocalDate startDate, LocalDate endDate);
}

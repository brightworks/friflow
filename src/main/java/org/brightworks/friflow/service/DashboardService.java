package org.brightworks.friflow.service;

import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.domain.dto.DeliveryDTO;
import org.brightworks.friflow.domain.dto.ProductionDTO;
import org.brightworks.friflow.domain.dto.QuotationDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.joda.time.LocalDate;

import java.util.List;

/**
 * Created by kyel on 12/25/2015.
 */
public interface DashboardService {

    List<ProductionDTO> getUrgentProduction(LocalDate localDate,ProcessStatus status);

    List<QuotationDTO> getUrgentQuotation(LocalDate localDate,ProcessStatus status);

    List<DeliveryDTO> getUrgentDelivery(LocalDate localDate,ProcessStatus status);

    List<CollectionDTO> getUrgentCollection(LocalDate localDate,ProcessStatus status);
}

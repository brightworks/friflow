package org.brightworks.friflow.service.delivery;

import org.brightworks.friflow.domain.dto.DeliveryDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.delivery.Delivery;
import org.brightworks.friflow.mapper.OrikaBeanMapper;
import org.brightworks.friflow.repo.delivery.DeliveryRepo;
import org.brightworks.friflow.service.FormDataIntegrityException;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author kyel
 */
@Transactional
@Service
public class DeliveryServiceImpl implements DeliveryService {

    @Autowired
    private DeliveryRepo deliveryRepo;

    @Autowired
    private OrikaBeanMapper mapper;

    @Override
    public Delivery save(DeliveryDTO deliveryDTO) throws FormDataIntegrityException {
        Delivery delivery;
        if(deliveryDTO.getId() != null){
            delivery = deliveryRepo.findOne(deliveryDTO.getId());
            if(delivery.getVersion() != deliveryDTO.getVersion()) throw new FormDataIntegrityException();
            mapper.map(deliveryDTO, delivery);
        }else{
            delivery = mapper.map(deliveryDTO,Delivery.class);

        }
        delivery = deliveryRepo.save(delivery);
        return delivery;
    }

    @Override
    public DeliveryDTO findOne(Long id) {
        return mapper.map(deliveryRepo.findOne(id), DeliveryDTO.class);
    }

    @Override
    public Page<DeliveryDTO> search(String ticketNumber, String description, String clientName, LocalDate startDate, LocalDate endDate,
                                    ProcessStatus processStatus, Pageable pageable) {
        return deliveryRepo.search(ticketNumber,description,ticketNumber,startDate,endDate,processStatus,pageable);
    }
}

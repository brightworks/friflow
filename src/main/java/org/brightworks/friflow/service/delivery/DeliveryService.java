package org.brightworks.friflow.service.delivery;

import org.brightworks.friflow.domain.dto.DeliveryDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.delivery.Delivery;
import org.brightworks.friflow.service.FormDataIntegrityException;
import org.joda.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author kyel
 */
public interface DeliveryService {

    Delivery save(DeliveryDTO deliveryDTO) throws FormDataIntegrityException;

    DeliveryDTO findOne(Long id);

    Page<DeliveryDTO> search(String ticketNumber,
                              String description,
                              String clientName,
                              LocalDate startDate,
                              LocalDate endDate,
                              ProcessStatus processStatus,
                              Pageable pageable);

}

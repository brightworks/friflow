package org.brightworks.friflow.service.impl;

import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.domain.dto.DeliveryDTO;
import org.brightworks.friflow.domain.dto.ProductionDTO;
import org.brightworks.friflow.domain.dto.QuotationDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.collection.Collection;
import org.brightworks.friflow.domain.process.delivery.Delivery;
import org.brightworks.friflow.domain.process.production.Production;
import org.brightworks.friflow.domain.process.quotation.Quotation;
import org.brightworks.friflow.mapper.OrikaBeanMapper;
import org.brightworks.friflow.repo.collection.CollectionRepo;
import org.brightworks.friflow.repo.delivery.DeliveryRepo;
import org.brightworks.friflow.repo.production.ProductionRepo;
import org.brightworks.friflow.repo.quotation.QuotationRepo;
import org.brightworks.friflow.service.DashboardService;
import org.brightworks.friflow.transformer.QuotationConverter;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kyel
 */
@Service
@Transactional
public class DashboardServiceImpl implements DashboardService {

    @Autowired
    private QuotationConverter quotationConverter;

    @Autowired
    private QuotationRepo quotationRepo;

    @Autowired
    private ProductionRepo productionRepo;

    @Autowired
    private CollectionRepo collectionRepo;

    @Autowired
    private DeliveryRepo deliveryRepo;

    @Autowired
    private OrikaBeanMapper mapper;

    @Override
    public List<ProductionDTO> getUrgentProduction(LocalDate localDate,ProcessStatus status) {
        List<Production> productions = productionRepo.findTop5ByTargetDateLessThanEqualAndProcessStatus(localDate,status);
        return convertDto(productions, new ArrayList<ProductionDTO>(), ProductionDTO.class);
    }

    @Override
    public List<QuotationDTO> getUrgentQuotation(LocalDate localDate,ProcessStatus status) {
        List<Quotation> quotations = quotationRepo.findTop5ByTargetDateLessThanEqualAndProcessStatus(localDate,status);
        List<QuotationDTO> dtos = new ArrayList<>();
        for(Quotation quotation: quotations){
            dtos.add(quotationConverter.toDTO(new QuotationDTO(), quotation));
        }
        return dtos;
    }

    @Override
    public List<DeliveryDTO> getUrgentDelivery(LocalDate localDate,ProcessStatus status) {
        List<Delivery> deliveries = deliveryRepo.findTop5ByTargetDateLessThanEqualAndProcessStatus(localDate,status);
        return convertDto(deliveries, new ArrayList<DeliveryDTO>(), DeliveryDTO.class);
    }

    @Override
    public List<CollectionDTO> getUrgentCollection(LocalDate localDate,ProcessStatus status) {
        List<Collection> collections = collectionRepo.findTop5ByTargetDateLessThanEqualAndProcessStatus(localDate,status);
        return convertDto(collections, new ArrayList<CollectionDTO>(), CollectionDTO.class);
    }

    private <ENTITY,DTO> List<DTO> convertDto(List<ENTITY> entities, List<DTO> dtos, Class<DTO> clazz){
        for(ENTITY e : entities){
            try {
                clazz.newInstance();
               dtos.add(mapper.map(e,clazz));
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            }
        }
        return dtos;

    }
}

package org.brightworks.friflow.service.impl;

import org.brightworks.friflow.domain.UserAccount;
import org.brightworks.friflow.domain.dto.UserAccountDTO;
import org.brightworks.friflow.mapper.OrikaBeanMapper;
import org.brightworks.friflow.repo.UserAccountRepo;
import org.brightworks.friflow.service.PasswordDoesNotMatchException;
import org.brightworks.friflow.service.UserAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kyel
 */
@Service
public class UserAccountServiceImpl implements UserAccountService{

    private static final Logger LOG = LoggerFactory.getLogger(UserAccountServiceImpl.class);

    @Autowired
    private UserAccountRepo userAccountRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private OrikaBeanMapper mapper;

    @Override
    @Transactional
    public void save(UserAccount user) {
        String hashedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
        userAccountRepo.save(user);
    }

    @Override
    @Transactional
    public void changePassword(String username, String oldPassword, String newPassword)
            throws PasswordDoesNotMatchException {

        UserAccount userAccount = userAccountRepo.findByUsername(username);

        if(passwordEncoder.matches(oldPassword,userAccount.getPassword())){
            userAccount.setPassword(passwordEncoder.encode(newPassword));
            userAccountRepo.save(userAccount);
        }else{
            throw new PasswordDoesNotMatchException();
        }
    }

    @Override
    public void update(UserAccount userAccount) {
        userAccountRepo.save(userAccount);
    }

    @Override
    public UserAccount findByUsername(String username) {
        LOG.info("Searching User Account using their username");
        return userAccountRepo.findByUsername(username);
    }

    public List<UserAccountDTO> retrieveAll(){
        LOG.info("Retrieving All User Accounts");
        List<UserAccountDTO> dto = new ArrayList<>();
        for(UserAccount account: userAccountRepo.findAll()){
            dto.add(mapper.map(account, UserAccountDTO.class));
        }
        return dto;
    }
}

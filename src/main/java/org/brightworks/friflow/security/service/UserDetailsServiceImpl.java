package org.brightworks.friflow.security.service;

import org.brightworks.friflow.domain.Role;
import org.brightworks.friflow.domain.UserAccount;
import org.brightworks.friflow.repo.UserAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kyel
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService{

    @Autowired
    private UserAccountRepo userAccountRepo;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        UserAccount user = userAccountRepo.findByUsername(username);

        if(user != null){
            return  new User(user.getUsername(),
                    user.getPassword(),
                    grantedAuthorities(user.getRoles()));
        }
        throw new UsernameNotFoundException("Bad Credentials");
    }

    private List<SimpleGrantedAuthority> grantedAuthorities(List<Role> roles){
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        for(Role r: roles){
            authorities.add(new SimpleGrantedAuthority(r.toString()));
        }
        return  authorities;
    }
}

package org.brightworks.friflow.transformer;

/**
 * @author kyel
 */
public interface ModelConverter<MODEL, DTO> {

    DTO toDTO(DTO dto, MODEL model);

    MODEL toModel(MODEL model,DTO dto);
}

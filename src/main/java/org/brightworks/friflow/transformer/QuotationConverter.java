package org.brightworks.friflow.transformer;

import org.brightworks.friflow.domain.CompanyName;
import org.brightworks.friflow.domain.dto.AttachmentDTO;
import org.brightworks.friflow.domain.dto.QuotationDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.attachment.AttachmentMetaData;
import org.brightworks.friflow.domain.process.quotation.Quotation;
import org.brightworks.friflow.repo.CompanyNameRepo;
import org.brightworks.friflow.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author kyel
 */
@Component
public class QuotationConverter implements ModelConverter<Quotation,QuotationDTO> {

    @Autowired
    private CompanyNameRepo companyNameRepo;

    @Override
    public QuotationDTO toDTO(QuotationDTO quotationDTO, Quotation quotation) {
        quotationDTO.setId(quotation.getId());
        quotationDTO.setVersion(quotation.getVersion());
        quotationDTO.setCustomerName(quotation.getCustomerName());
        quotationDTO.setItemDescription(quotation.getItemDescription());

        quotationDTO.setPrice(quotation.getPrice() != null ? quotation.getPrice().doubleValue(): null);
        quotationDTO.setTargetDate(DateUtil.formatDate(quotation.getTargetDate()));
        quotationDTO.setStatus(quotation.getProcessStatus().toString());
        quotationDTO.setTicketNumber(quotation.getTicketNumber());
            quotationDTO.setCreatedDateTime(DateUtil.formatDateTime(quotation.getDateTimeCreated(), "MM-dd-yyyy hh:mm aa"));
        List<AttachmentDTO> attachmentMetaDatas = new ArrayList<>();
        for(AttachmentMetaData attachment: quotation.getAttachments()){
            if(!attachment.isDeleted()){
                AttachmentDTO dto = new AttachmentDTO();
                dto.setFileName(attachment.getFileName());
                dto.setAttachmentId(attachment.getAttachment().getId().toString());
                dto.setContentType(attachment.getContentType());
                dto.setAttachmentMetaDataId(attachment.getId().toString());
                attachmentMetaDatas.add(dto);
            }
        }

        if(quotation.getProcessStatus() != null
                && (quotation.getProcessStatus()
                .equals(ProcessStatus.COMPLETED))) {
            quotationDTO.setEditable(false);
        }

        quotationDTO.setAttachments(!attachmentMetaDatas.isEmpty() ? attachmentMetaDatas : null );
        return quotationDTO;
    }

    public Quotation toModel(Quotation quotation, QuotationDTO dto){
        quotation.setCustomerName(dto.getCustomerName());
        quotation.setItemDescription(dto.getItemDescription());
        //quotation.setVersion(dto.getVersion());
        quotation.setPrice(dto.getPrice() != null ?  new BigDecimal(dto.getPrice()) : null);
        quotation.setTargetDate(DateUtil.formatDate(dto.getTargetDate()));
        quotation.setProcessStatus(ProcessStatus.valueOf(dto.getStatus()));
        setQuotationClientName(dto, quotation);
        return quotation;
    }


    private void setQuotationClientName(QuotationDTO dto, Quotation quotation) {
        if(doesCompanyNameExist(dto.getCustomerName())){
            quotation.setClientName(companyNameRepo.findByName(dto.getCustomerName().toUpperCase()));
        }else{
            CompanyName companyName = new CompanyName();
            companyName.setName(dto.getCustomerName().toUpperCase());
            quotation.setClientName(companyNameRepo.save(companyName));
        }
    }

    private boolean doesCompanyNameExist(String companyName){
        return (companyNameRepo.findByName(companyName.toUpperCase()) != null);
    }
}

package org.brightworks.friflow.controller;

import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.domain.dto.DeliveryDTO;
import org.brightworks.friflow.domain.dto.ProductionDTO;
import org.brightworks.friflow.domain.dto.QuotationDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.service.DashboardService;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author kyel
 */
@Controller
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    @RequestMapping({"/dashboard","/"})
    public ModelAndView dashboard(){
        ModelAndView modelAndView = new ModelAndView("index");
        LocalDate weekDate = LocalDate.now();
        List<CollectionDTO> collections = dashboardService.getUrgentCollection(weekDate, ProcessStatus.PENDING);
        List<ProductionDTO> productionDTOs = dashboardService.getUrgentProduction(weekDate, ProcessStatus.PENDING);
        List<QuotationDTO> quotationDTOs = dashboardService.getUrgentQuotation(weekDate, ProcessStatus.PENDING);
        List<DeliveryDTO> deliveryDTOs = dashboardService.getUrgentDelivery(weekDate, ProcessStatus.PENDING);
        modelAndView.addObject("collection",collections);
        modelAndView.addObject("production",productionDTOs);
        modelAndView.addObject("quotation",quotationDTOs);
        modelAndView.addObject("delivery",deliveryDTOs);

        modelAndView.addObject("deliveryLength", deliveryDTOs.size());
        modelAndView.addObject("collectionLength",collections.size());
        modelAndView.addObject("productionLength", productionDTOs.size());
        modelAndView.addObject("quotationLength",quotationDTOs.size());
        return modelAndView;
    }

}

package org.brightworks.friflow.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author kyel
 */
@Controller
public class LoginController {

    @RequestMapping("/login")
    public String login(){
        return  "login";
    }

}

package org.brightworks.friflow.controller.collection;

import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.domain.process.collection.PaymentTerm;
import org.brightworks.friflow.service.collection.CollectionService;
import org.brightworks.friflow.util.DataTablesResponse;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kyel on 12/28/2015.
 */

@RestController
public class CollectionSearchController {

    @Autowired
    private CollectionService collectionService;
    @RequestMapping("/collection/ajax/search")
    public DataTablesResponse<CollectionDTO> searchQuotations(@RequestParam(required=false) int sEcho,
                                                             @RequestParam (required=false) int iDisplayStart,
                                                             @RequestParam (required=false) int iDisplayLength,
                                                             @RequestParam (required=false) int iColumns,
                                                             @RequestParam (required=false) int iSortCol_0,
                                                             @RequestParam (required=false)String sSortDir_0,
                                                             @RequestParam (required=false) String invoiceNumber,
                                                             @RequestParam (required=false) String clientName,
                                                             @RequestParam (required=false) String description,
                                                             @RequestParam(required=false) String paymentTerm,
                                                              @RequestParam(required=false) ProcessStatus processStatus,
                                                             @RequestParam (required=false) String startDate,
                                                             @RequestParam (required=false) String endDate){
        int page = (int) Math.ceil(iDisplayStart/iDisplayLength);
        LocalDate sDate =  !startDate.isEmpty() ?  LocalDate.parse(startDate, DateTimeFormat.forPattern("MM-dd-yyyy")) : null;
        LocalDate eDate = !endDate.isEmpty() ?  LocalDate.parse(endDate, DateTimeFormat.forPattern("MM-dd-yyyy")) : null;
        PaymentTerm term = (paymentTerm == null) ? PaymentTerm.NEW_CUSTOMER : PaymentTerm.valueOf(paymentTerm);
        Page<CollectionDTO> collectionDTOPage =  collectionService.search(invoiceNumber,
                description,
                clientName,
                sDate,
                eDate,
                processStatus,
                term,
                iDisplayLength != -1 ? new PageRequest(page, iDisplayLength) : null);
        return new DataTablesResponse<>(collectionDTOPage.getContent(),
                sEcho,
                collectionDTOPage.getTotalElements(),
                collectionDTOPage.getTotalElements());


    }
}

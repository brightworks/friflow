package org.brightworks.friflow.controller.collection;

import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.domain.process.collection.Collection;
import org.brightworks.friflow.service.FormDataIntegrityException;
import org.brightworks.friflow.service.collection.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author mil
 */
@Controller
@RequestMapping("/collection")
public class CollectionController {

    @Autowired
    private CollectionService collectionService;

    @RequestMapping("/new")
    public ModelAndView newCollection(){
        ModelAndView mav = new ModelAndView("collection/collection_form");
        mav.addObject("collection", new CollectionDTO());
        return mav;
    }

    @RequestMapping("/view/{id}")
    public ModelAndView viewCollection(@PathVariable("id") Long id)  {
        ModelAndView mav = new ModelAndView("collection/collection_view");
        mav.addObject("collection", collectionService.findOne(id));
        return mav;
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView editCollection(@PathVariable("id") Long id)  {
        ModelAndView mav = new ModelAndView("collection/collection_form");
        mav.addObject("collection", collectionService.findOne(id));
        return mav;
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute CollectionDTO collectionDTO){
        Collection collection = null;
        try {
            collection = collectionService.save(collectionDTO);
        } catch (FormDataIntegrityException e) {
            ModelAndView mav = new ModelAndView("collection/collection_form");
            mav.addObject(collectionService.findOne(collectionDTO.getId()));
            mav.addObject("error", "Ticket has been recently updated, Updating the form with recent data");
            return mav;
        }
        return new ModelAndView("redirect:/collection/view/"+collection.getId());
    }

    @RequestMapping("/search")
    public ModelAndView displaySearch(Pageable pageable){
        return new ModelAndView("collection/collection_list");
    }

}

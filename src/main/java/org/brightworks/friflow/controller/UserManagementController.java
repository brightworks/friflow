package org.brightworks.friflow.controller;

import org.brightworks.friflow.domain.Role;
import org.brightworks.friflow.domain.UserAccount;
import org.brightworks.friflow.domain.dto.PasswordEditDTO;
import org.brightworks.friflow.domain.dto.UserAccountDTO;
import org.brightworks.friflow.mapper.OrikaBeanMapper;
import org.brightworks.friflow.service.PasswordDoesNotMatchException;
import org.brightworks.friflow.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author kyel
 */
@Controller
@RequestMapping("/user")
@SessionAttributes("user")
public class UserManagementController {

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private OrikaBeanMapper mapper;

    @RequestMapping(value = "/new")
    public ModelAndView showUserManagementForm(){
        ModelAndView mav = new ModelAndView("user/user_form");
        mav.addObject("user",new UserAccountDTO());
        mav.addObject("roles", Role.values());
        return mav;
    }

    @RequestMapping(value = "/edit/{username}")
    public ModelAndView editUser(@PathVariable("username") String username){
        ModelAndView mav = new ModelAndView("user/user_form");
        UserAccount account = userAccountService.findByUsername(username);
        UserAccountDTO userAccountDTO = new UserAccountDTO();
        mapper.map(account, userAccountDTO);
        mav.addObject("user",userAccountDTO);
        mav.addObject("roles", Role.values());
        return  mav;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView saveUser(@ModelAttribute UserAccountDTO user){

        UserAccount userAccount;

        try{
            userAccount =  userAccountService.findByUsername(user.getUsername());
        }catch (Exception e){
            ModelAndView mav = new ModelAndView("user/user_form");
            mav.addObject("user",user);
            mav.addObject("user_name_error","User Name Already Exists");
            mav.addObject("roles", Role.values());
            return  mav;
        }

        if(user.getId() != null){
         userAccount =  userAccountService.findByUsername(user.getUsername());
        }else{
            userAccount = new UserAccount();
        }

        mapper.map(user, userAccount);
        if(userAccount.getId() == null){
            userAccountService.save(userAccount);
        }else{
            userAccountService.update(userAccount);
        }

        return new ModelAndView("redirect:/user/list?success=true");
    }

    @RequestMapping("/list")
    public ModelAndView listUsers(@RequestParam(required = false)boolean success) {
        List<UserAccountDTO> users = userAccountService.retrieveAll();
        ModelAndView mav = new ModelAndView("user/user_list");
        if(success){
            mav.addObject("message","Successfully Saved User");
        }
        mav.addObject("users", users);
        return mav;
    }

    @RequestMapping(value = "/edit-password/{username}")
    public ModelAndView showEditPassword(@PathVariable("username") String username){
        ModelAndView mav = new ModelAndView("user/password-edit-form");
        PasswordEditDTO passwordEditDTO = new PasswordEditDTO();
        passwordEditDTO.setUsername(username);
        mav.addObject("user",passwordEditDTO);
        return  mav;
    }

    @RequestMapping(value = "/edit-password/save", method = RequestMethod.POST)
    public ModelAndView saveEditPassword(@ModelAttribute PasswordEditDTO passwordEditDTO){
        try {
            userAccountService.changePassword(passwordEditDTO.getUsername(),passwordEditDTO.getOldPassword(),passwordEditDTO.getNewPassword());
        } catch (PasswordDoesNotMatchException e) {
            ModelAndView mav = new ModelAndView("user/password-edit-form");
            PasswordEditDTO newPasswordEditDTO = new PasswordEditDTO();
            newPasswordEditDTO.setUsername(passwordEditDTO.getUsername());
            mav.addObject("user",newPasswordEditDTO);
            mav.addObject("password-error","Password is incorrect");
            return  mav;
        }
        return new ModelAndView("redirect:/user/list?success=true");
    }

}

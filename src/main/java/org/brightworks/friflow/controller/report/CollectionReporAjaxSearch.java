package org.brightworks.friflow.controller.report;

import org.brightworks.friflow.domain.dto.CollectionDTO;
import org.brightworks.friflow.service.collection.CollectionService;
import org.brightworks.friflow.util.DataTablesResponse;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kyel on 2/7/2016.
 */
@RestController
public class CollectionReporAjaxSearch {

    @Autowired
    private CollectionService collectionService;


    @RequestMapping("/report/retrieve/all")
    public DataTablesResponse<CollectionDTO> searchQuotations(@RequestParam(required=false) int sEcho,
                                                              @RequestParam (required=false) int iDisplayStart,
                                                              @RequestParam (required=false) int iDisplayLength,
                                                              @RequestParam (required=false) int iColumns,
                                                              @RequestParam (required=false) int iSortCol_0,
                                                              @RequestParam (required=false)String sSortDir_0,
                                                              @RequestParam (required=false) String startDate,
                                                              @RequestParam (required=false) String endDate){

        int page = (int) Math.ceil(iDisplayStart/iDisplayLength);

        if(startDate.isEmpty()  && endDate.isEmpty()){
            return new DataTablesResponse<>(new ArrayList<CollectionDTO>(),sEcho,0,0);
        }

        LocalDate sDate =  !startDate.isEmpty() ?  LocalDate.parse(startDate, DateTimeFormat.forPattern("MM-dd-yyyy")) : null;
        LocalDate eDate = !endDate.isEmpty() ?  LocalDate.parse(endDate, DateTimeFormat.forPattern("MM-dd-yyyy")) : null;
        List<CollectionDTO> collectionDTOPage  =  collectionService.retrieveCollectionBetweenDates(sDate,eDate);

        return new DataTablesResponse<>(collectionDTOPage,
                sEcho,
                collectionDTOPage.size(),
                collectionDTOPage.size());


    }
}

package org.brightworks.friflow.controller.report;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by kyel on 2/7/2016.
 */
@Controller
public class ReportController {

    @RequestMapping("/report")
    public ModelAndView displayReportPage(){
        ModelAndView mav = new ModelAndView("report");
        return mav;
    }
}

package org.brightworks.friflow.controller;

import org.brightworks.friflow.domain.Name;
import org.brightworks.friflow.domain.Role;
import org.brightworks.friflow.domain.UserAccount;
import org.brightworks.friflow.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Serves as initialization Controller
 * Provides the default accounts for each application modules
 * admin:123qwe,
 * quotation:123qwe,
 * production:123qwe,
 * delivery:123qwe,
 * collection:123qwe
 * @author kyel
 */
@Controller
public class InitController {

    @Autowired
    private UserAccountService userAccountService;


    @PostConstruct
    public void initAccount(){

        UserAccount account = userAccountService.findByUsername("admin");

        if(account == null){
            UserAccount userAccount = new UserAccount();
            userAccount.setUsername("admin");
            userAccount.setPassword("123qwe");
            Name name = new Name();
            name.setGivenName("Admin");
            name.setFamilyName("Admin");
            name.setMiddleName("Admin");
            userAccount.setName(name);
            List<Role> roles = new ArrayList<Role>();

            roles.add(Role.ROLE_ADMIN);
            roles.add(Role.ROLE_COLLECTION_ADD);
            roles.add(Role.ROLE_COLLECTION_EDIT);
            roles.add(Role.ROLE_COLLECTION_VIEW);
            roles.add(Role.ROLE_DELIVERY_ADD);
            roles.add(Role.ROLE_DELIVERY_EDIT);
            roles.add(Role.ROLE_DELIVERY_VIEW);
            roles.add(Role.ROLE_PRODUCTION_ADD);
            roles.add(Role.ROLE_PRODUCTION_EDIT);
            roles.add(Role.ROLE_PRODUCTION_VIEW);
            roles.add(Role.ROLE_QUOTATION_ADD);
            roles.add(Role.ROLE_QUOTATION_EDIT);
            roles.add(Role.ROLE_QUOTATION_VIEW);
            userAccount.setRoles(roles);
            userAccountService.save(userAccount);
        }


        UserAccount quotation = userAccountService.findByUsername("quotation");

        if(quotation == null){
            quotation = new UserAccount();
            quotation.setUsername("quotation");
            quotation.setPassword("123qwe");

            Name name = new Name();
            name.setGivenName("Quotation");
            name.setFamilyName("Quotation");
            name.setMiddleName("Quotation");
            quotation.setName(name);

            List<Role> roles = new ArrayList<Role>();
            roles.add(Role.ROLE_QUOTATION_ADD);
            roles.add(Role.ROLE_QUOTATION_EDIT);
            roles.add(Role.ROLE_QUOTATION_VIEW);
            roles.add(Role.ROLE_DELIVERY_VIEW);
            roles.add(Role.ROLE_COLLECTION_VIEW);
            roles.add(Role.ROLE_PRODUCTION_VIEW);
            quotation.setRoles(roles);
            userAccountService.save(quotation);
        }

        UserAccount production = userAccountService.findByUsername("production");

        if(production == null){
            production= new UserAccount();
            production.setUsername("production");
            production.setPassword("123qwe");
            Name name = new Name();
            name.setGivenName("Production");
            name.setFamilyName("Production");
            name.setMiddleName("Production");
            production.setName(name);

            List<Role> roles = new ArrayList<Role>();
            roles.add(Role.ROLE_PRODUCTION_ADD);
            roles.add(Role.ROLE_PRODUCTION_EDIT);
            roles.add(Role.ROLE_PRODUCTION_VIEW);
            roles.add(Role.ROLE_QUOTATION_VIEW);
            roles.add(Role.ROLE_DELIVERY_VIEW);
            roles.add(Role.ROLE_COLLECTION_VIEW);
            production.setRoles(roles);
            userAccountService.save(production);
        }


        UserAccount collection = userAccountService.findByUsername("collection");

        if(collection == null){
            collection = new UserAccount();
            collection.setUsername("collection");
            collection.setPassword("123qwe");

            Name name = new Name();
            name.setGivenName("Collection");
            name.setFamilyName("Collection");
            name.setMiddleName("Collection");
            collection.setName(name);

            List<Role> roles = new ArrayList<Role>();
            roles.add(Role.ROLE_COLLECTION_ADD);
            roles.add(Role.ROLE_COLLECTION_EDIT);
            roles.add(Role.ROLE_COLLECTION_VIEW);
            roles.add(Role.ROLE_QUOTATION_VIEW);
            roles.add(Role.ROLE_DELIVERY_VIEW);
            roles.add(Role.ROLE_PRODUCTION_VIEW);
            collection.setRoles(roles);
            userAccountService.save(collection);
        }


        UserAccount delivery = userAccountService.findByUsername("delivery");

        if(delivery == null){
            delivery = new UserAccount();
            delivery.setUsername("delivery");
            delivery.setPassword("123qwe");


            Name name = new Name();
            name.setGivenName("Delivery");
            name.setFamilyName("Delivery");
            name.setMiddleName("Delivery");
            delivery.setName(name);

            List<Role> roles = new ArrayList<Role>();
            roles.add(Role.ROLE_DELIVERY_ADD);
            roles.add(Role.ROLE_DELIVERY_EDIT);
            roles.add(Role.ROLE_DELIVERY_VIEW);
            roles.add(Role.ROLE_QUOTATION_VIEW);
            roles.add(Role.ROLE_COLLECTION_VIEW);
            roles.add(Role.ROLE_PRODUCTION_VIEW);
            delivery.setRoles(roles);
            userAccountService.save(delivery);
        }
    }

}

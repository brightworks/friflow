package org.brightworks.friflow.controller.delivery;

import org.brightworks.friflow.domain.dto.DeliveryDTO;
import org.brightworks.friflow.domain.process.ProcessStatus;
import org.brightworks.friflow.service.delivery.DeliveryService;
import org.brightworks.friflow.util.DataTablesResponse;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author kyel
 */
@RestController
public class DeliverySearchController {

    @Autowired
    private DeliveryService deliveryService;

    @RequestMapping("/delivery/ajax/search")
    public DataTablesResponse<DeliveryDTO> searchQuotations(@RequestParam(required=false) int sEcho,
                                                              @RequestParam (required=false) int iDisplayStart,
                                                              @RequestParam (required=false) int iDisplayLength,
                                                              @RequestParam (required=false) int iColumns,
                                                              @RequestParam (required=false) int iSortCol_0,
                                                              @RequestParam (required=false)String sSortDir_0,
                                                              @RequestParam (required=false) String ticketNumber,
                                                              @RequestParam (required=false) String jobOrderNumber,
                                                              @RequestParam (required=false) String purchaseNumber,
                                                              @RequestParam (required=false) String clientName,
                                                              @RequestParam (required=false) String description,
                                                              @RequestParam (required=false) String startDate,
                                                              @RequestParam (required=false) String endDate,
                                                             @RequestParam(required = false)ProcessStatus status){

        int page = (int) Math.ceil(iDisplayStart/iDisplayLength);

        LocalDate start = !startDate.isEmpty() ?  LocalDate.parse(startDate, DateTimeFormat.forPattern("MM-dd-yyyy")) : null;
        LocalDate end =  !endDate.isEmpty() ?  LocalDate.parse(endDate, DateTimeFormat.forPattern("MM-dd-yyyy")) : null;

        Page<DeliveryDTO> deliveryDTOs = deliveryService.search(ticketNumber,description,clientName,start,end,status,iDisplayLength != -1 ? new PageRequest(page, iDisplayLength) : null);
        return new DataTablesResponse<>(deliveryDTOs.getContent(),
                sEcho,
                deliveryDTOs.getTotalElements(),
                deliveryDTOs.getTotalElements());
    }
}

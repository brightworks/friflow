package org.brightworks.friflow.controller.delivery;

import org.brightworks.friflow.domain.dto.DeliveryDTO;
import org.brightworks.friflow.domain.dto.ProductionDTO;
import org.brightworks.friflow.domain.process.delivery.Delivery;
import org.brightworks.friflow.service.FormDataIntegrityException;
import org.brightworks.friflow.service.delivery.DeliveryService;
import org.brightworks.friflow.service.production.ProductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author kyel
 */
@Controller
@RequestMapping("/delivery")
public class DeliveryController {

    @Autowired
    private DeliveryService deliveryService;

    @Autowired
    private ProductionService productionService;

    @RequestMapping("/new")
    public ModelAndView newDelivery(){
        ModelAndView mav = new ModelAndView("delivery/delivery_form");
        mav.addObject("delivery", new DeliveryDTO());
        return mav;
    }

    @RequestMapping("/new/{ticketNumber}")
    public ModelAndView newProduction(@PathVariable("ticketNumber") String ticketNumber)  {
        ModelAndView mav = new ModelAndView("delivery/delivery_form");
        ProductionDTO productionDTO = productionService.findOne(ticketNumber);
        DeliveryDTO deliveryDTO = new DeliveryDTO();
        deliveryDTO.setCustomerName(productionDTO.getCustomerName());
        deliveryDTO.setProductionTicketNumber(productionDTO.getTicketNumber());
        mav.addObject("delivery", deliveryDTO);
        mav.addObject("from_production",true);
        return mav;
    }

    @RequestMapping("/view/{id}")
    public ModelAndView viewDelivery(@PathVariable("id") Long id)  {
        ModelAndView mav = new ModelAndView("delivery/delivery_view");
        mav.addObject("delivery", deliveryService.findOne(id));
        return mav;
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView editDelivery(@PathVariable("id") Long id)  {
        ModelAndView mav = new ModelAndView("delivery/delivery_form");
        mav.addObject("delivery", deliveryService.findOne(id));
        return mav;
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public ModelAndView save(@ModelAttribute DeliveryDTO deliveryDTO){
        Delivery delivery = null;
        try {
            delivery = deliveryService.save(deliveryDTO);
        } catch (FormDataIntegrityException e) {
            ModelAndView mav = new ModelAndView("delivery/delivery_form");
            mav.addObject(deliveryService.findOne(deliveryDTO.getId()));
            mav.addObject("error", "Ticket has been recently updated, Updating the form with recent data");
            return mav;
        }
        return new ModelAndView("redirect:/delivery/view/"+delivery.getId());
    }

    @RequestMapping("/search")
    public ModelAndView displaySearch(Pageable pageable){
        return new ModelAndView("delivery/delivery_list");
    }
}

package org.brightworks.friflow.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * author kyel
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable().formLogin().loginPage("/login")
                .passwordParameter("j_password")
                .usernameParameter("j_username")
                .defaultSuccessUrl("/dashboard")
                .and()
                .authorizeRequests()
                .antMatchers("/resources*//**", "/login")
                .permitAll()
                .antMatchers("/", "/dashboard")
                .hasAnyRole("ADMIN",
                        "QUOTATION",
                        "PRODUCTION",
                        "COLLECTION",
                        "DELIVERY",
                        "QUOTATION_VIEW",
                        "QUOTATION_EDIT",
                        "DELIVERY_VIEW",
                        "DELIVERY_EDIT",
                        "COLLECTION_VIEW",
                        "COLLECTION_EDIT",
                        "PRODUCTION_VIEW",
                        "PRODUCTION_EDIT");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }
}

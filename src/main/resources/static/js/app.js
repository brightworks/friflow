

function deleteTicket(dataTable,url) {

    var result = confirm("Are you sure you want to delete the ticket?");

    if(result == true) {
        $.get(url, function( data ) {
            dataTable.fnDraw();
            deleteNotification()
        });
    }
}

function deleteNotification() {
    $.notify({
        message: 'Successfully Deleted Ticket'
    }, {
        type: 'success',
        delay: 5000
    });
}
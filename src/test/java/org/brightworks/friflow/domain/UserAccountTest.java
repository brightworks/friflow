package org.brightworks.friflow.domain;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.brightworks.friflow.domain.Role.*;
import static org.junit.Assert.*;

/**
 * @author kyel
 */
public class UserAccountTest {

    @Test
    public void userAccountWithNoNameTest(){
        UserAccount userAccount = new UserAccount();
        assertEquals(userAccount.getName(),null);
    }

    @Test
    public void userAccountWithNameTest(){
        UserAccount userAccount = new UserAccount();
        Name name = new Name();
        userAccount.setName(name);
        assertNotNull(userAccount.getName());
    }

    @Test
    public void userAccountWithUsernameAndPasswordTest(){
        UserAccount userAccount = new UserAccount();
        userAccount.setUsername("username");
        userAccount.setPassword("password");
        assertNotNull(userAccount.getUsername());
        assertNotNull(userAccount.getPassword());
    }

    @Test
    public void userAccountNameNonNullDefaultFieldValuesTest(){
        UserAccount userAccount = new UserAccount();
        userAccount.setName(new Name());
        assertNotNull(userAccount.getName().getFamilyName());
        assertNotNull(userAccount.getName().getGivenName());
        assertNotNull(userAccount.getName().getMiddleName());
    }

    @Test
    public void userAccountWithQuotationRole(){
        UserAccount userAccount = new UserAccount();
        List<Role> accountRoles = new ArrayList<>();
        accountRoles.add(ROLE_QUOTATION_ADD);
        userAccount.setRoles(accountRoles);
        assertThat(accountRoles,Matchers.hasItem(ROLE_QUOTATION_ADD));
    }

    @Test
    public void userAccountWithAProductRole(){
        UserAccount userAccount = new UserAccount();
        List<Role> accountRoles = new ArrayList<>();
        accountRoles.add(ROLE_PRODUCTION_ADD);
        userAccount.setRoles(accountRoles);
        assertThat(accountRoles,Matchers.hasItem(ROLE_PRODUCTION_ADD));
    }

    @Test
    public void userAccountWithACollectionRole(){
        UserAccount userAccount = new UserAccount();
        List<Role> accountRoles = new ArrayList<>();
        accountRoles.add(ROLE_COLLECTION_ADD);
        userAccount.setRoles(accountRoles);
        assertThat(accountRoles,Matchers.hasItem(ROLE_COLLECTION_ADD));
    }
    @Test
    public void userAccountWithADeliveryRole(){
        UserAccount userAccount = new UserAccount();
        List<Role> accountRoles = new ArrayList<>();
        accountRoles.add(ROLE_DELIVERY_ADD);
        userAccount.setRoles(accountRoles);
        assertThat(accountRoles,Matchers.hasItem(ROLE_DELIVERY_ADD));
    }

    @Test
    public void userAccountWithAnAdminRole(){
        UserAccount userAccount = new UserAccount();
        List<Role> accountRoles = new ArrayList<>();
        accountRoles.add(ROLE_ADMIN);
        userAccount.setRoles(accountRoles);
        assertThat(accountRoles,Matchers.hasItem(ROLE_ADMIN));
    }


    @Test
    public void userAccountWithAllRoles(){
        UserAccount userAccount = new UserAccount();
        List<Role> accountRoles = new ArrayList<>();
        accountRoles.add(ROLE_DELIVERY_ADD);
        accountRoles.add(ROLE_QUOTATION_ADD);
        accountRoles.add(ROLE_PRODUCTION_ADD);
        accountRoles.add(ROLE_COLLECTION_ADD);
        userAccount.setRoles(accountRoles);
        assertThat(accountRoles,Matchers.hasItems(ROLE_DELIVERY_ADD,
                ROLE_QUOTATION_ADD,
                ROLE_COLLECTION_ADD,
                ROLE_PRODUCTION_ADD));
    }
}

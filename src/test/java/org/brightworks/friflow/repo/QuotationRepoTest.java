package org.brightworks.friflow.repo;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.brightworks.friflow.Application;
import org.brightworks.friflow.domain.process.quotation.Quotation;
import org.brightworks.friflow.repo.quotation.QuotationRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static org.junit.Assert.assertEquals;
/**
 * @author kdavid
 */
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@DatabaseSetup(QuotationRepoTest.DATASET)
@DatabaseTearDown(type = DatabaseOperation.CLEAN_INSERT, value = { QuotationRepoTest.DATASET })
@DirtiesContext
public class QuotationRepoTest {

    protected static final String DATASET = "classpath:datasets/quotation-items.xml";

    private static final String ticket_number = "Q0000001";

    private static final Long ID = 1L;

    @Autowired
    private QuotationRepo quotationRepo;


    @Test
    public void findOne(){
        Quotation quotation = quotationRepo.findOne(ID);
        assertEquals(quotation.getId(),ID);
    }
}
